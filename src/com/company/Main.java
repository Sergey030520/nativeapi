package com.company;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Scanner;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;


public class Main {
    public static void main(String[] args){
        String commandUsr;
        try {
            NativeAPI nativeAPI = new NativeAPI(args);
            do {
                System.out.print("Native API: ");
                commandUsr = new Scanner(System.in).nextLine();
                String[] commandSplit = commandUsr.split(" ");
                switch (commandSplit[0]){
                    case "ls":
                        nativeAPI.lsNativeAPI();
                        break;
                    case "mkdir":
                        nativeAPI.mkdirNativeAPI(commandSplit[1]);
                        break;
                    case "cd":
                        nativeAPI.cdNativeAPI(commandSplit[1]);
                        break;
                    case "delete":
                        nativeAPI.deleteNativeAPI(commandSplit[1]);
                        break;
                    case "lcd":
                        nativeAPI.cdLocal(commandSplit[1]);
                        break;
                    case "lls":
                        nativeAPI.lsLocal();
                        break;
                    case "put":
                        nativeAPI.putNativeAPI(commandSplit[1]);
                        break;
                    case "get":
                        nativeAPI.get(commandSplit[1]);
                        break;
                    case "append":
                        nativeAPI.append(commandSplit);
                        break;
                    case "help":
                        nativeAPI.showAllCommand();
                        break;
                    case "create":
                        nativeAPI.create(commandSplit[1]);
                        break;
                    case "createLocalFile":
                        nativeAPI.createFileLocal(commandSplit[1]);
                        break;
                    default:
                        break;
                }
            } while (!commandUsr.equals("exit"));
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public static class NativeAPI {
        public static final String ANSI_RESET = "\u001B[0m";
        public static final String ANSI_RED = "\u001B[31m";
        public static final String ANSI_YELLOW = "\u001B[33m";
        private Path pathHdfs;
        private Path pathLocal;
        private FileSystem fileSystem;

        private Boolean CheckArgument(String arg, Boolean commandCD){
            if(arg.equals("..") && commandCD) return true;
            for (Character symbol : arg.toCharArray()) {
                if(symbol.equals('/')) return false;
            }
            return true;
        }
        public NativeAPI(String[] inParamAccess) {
            pathHdfs = new Path("/");
            pathLocal = new Path("/home/sergeimakarov/");
            Configuration conf = new Configuration();
            conf.set("fs.defaultFS", "hdfs://" + inParamAccess[0] + ":" + inParamAccess[1]);
            conf.set("dfs.replication", "1");
            try {
                fileSystem = FileSystem.get(conf);
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        }

        public void cdNativeAPI(String parameter) {
            if(!CheckArgument(parameter, true)){
                System.out.println("Error: the arguments cannot contain the '/ ' character!");
                return;
            }
            try {
                if (parameter.equals("..")) {
                    if (pathHdfs.getParent() != null) pathHdfs = pathHdfs.getParent();
                } else {
                    Path newPath = new Path((pathHdfs.toString().charAt(pathHdfs.toString().length() - 1) != '/'
                            ? pathHdfs.toString() + "/" : pathHdfs.toString()) + parameter);
                    if (fileSystem.exists(newPath)) {
                        pathHdfs = newPath;
                        System.out.println(newPath);
                    }
                    else System.out.println("Directory is not found!");
                }
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        }

        public void lsNativeAPI() {
            try {
                FileStatus[] fileStatus = fileSystem.listStatus(pathHdfs);
                for (FileStatus status : fileStatus) {
                    System.out.println(status.getPath().toString());
                }
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        }

        public void mkdirNativeAPI(String nameMkdir) {
            if(!CheckArgument(nameMkdir, false)){
                System.out.println("Error: the directory name cannot contain the character'/','..'!");
                return;
            }
            pathHdfs = new Path((pathHdfs.toString().charAt(pathHdfs.toString().length() - 1) != '/'
                    ? pathHdfs.toString() + "/" : pathHdfs.toString()) + nameMkdir);
            try {
                fileSystem.mkdirs(pathHdfs);
                System.out.println("Directory is created!");
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
                pathHdfs = pathHdfs.getParent();
            }
        }

        public void deleteNativeAPI(String parameter) {
            Path pathDel = new Path((pathHdfs.toString().charAt(pathHdfs.toString().length() - 1) != '/'
                    ? pathHdfs.toString() + "/" : pathHdfs.toString()) + parameter);
            try {
                fileSystem.delete(pathDel, true);
                System.out.println("The directory or file has been deleted!");
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        }

        public void cdLocal(String parameter) {
            try {
                Path newPath = new Path((pathLocal.toString().charAt(pathLocal.toString().length() - 1) != '/'
                        ? pathLocal.toString() + "/" : pathLocal.toString()) + parameter);
                if (parameter.equals("..")) {
                    if (pathLocal.getParent() != null) pathLocal = pathLocal.getParent();
                } else {
                    File dir = new File(newPath.toString());
                    if (dir.isDirectory()) {
                        pathLocal = newPath;
                        System.out.println(pathLocal);
                    }
                }
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        }
        public void createFileLocal(String nameFile){
            java.nio.file.Path newPath =  Paths.get(pathLocal.toString(), nameFile);
            if (!Files.exists(newPath)) {
                try {
                    Files.createFile(newPath);
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            }
        }
        public void lsLocal() {
            try {
                File dir = new File(pathLocal.toString()); //path указывает на директорию
                System.out.println("Files:");
                for (File file : Objects.requireNonNull(dir.listFiles())) {
                    if (file.isFile()) {
                        System.out.println(ANSI_YELLOW + file.getName() + ANSI_RESET);
                    }
                }
                System.out.println("Directory:");
                for (File file : Objects.requireNonNull(dir.listFiles())) {
                    if (!file.isFile()) {
                        System.out.println(ANSI_RED + file.getName() + ANSI_RESET);
                    }
                }
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        }

        public void putNativeAPI(String parameters) {
            try {
                Path newPath = new Path((pathLocal.toString().charAt(pathLocal.toString().length() - 1) != '/'
                        ? pathLocal.toString() + "/" : pathLocal.toString()) + parameters);
                fileSystem.copyFromLocalFile(newPath, pathHdfs);
                System.out.println("The file was uploaded successfully!");
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        }

        public void get(String nameFile) {
            try {
                Path newPath = new Path((pathHdfs.toString().charAt(pathHdfs.toString().length() - 1) != '/'
                        ? pathHdfs.toString() + "/" : pathHdfs.toString()) + nameFile);
                fileSystem.copyToLocalFile(newPath, pathLocal);
                System.out.println("The file was downloaded successfully!");
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
        }
        public void create(String nameFile){
            try{
                Path newPath = new Path((pathHdfs.toString().charAt(pathHdfs.toString().length() - 1) != '/'
                        ? pathHdfs.toString() + "/" : pathHdfs.toString()) + nameFile);
                fileSystem.create(new Path(newPath.toString()));
            }catch (Exception exception){
                System.out.println(exception.getMessage());
            }
        }
        /*public boolean deleteLocalFile(String nameFile){
            java.nio.file.Path newPath =  Paths.get(pathLocal.toString(), nameFile);
            if (Files.exists(newPath)) {
                try {
                    Files.delete(newPath);
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            }
            return true;
        }
        public boolean existLocalFile(String nameFile){
            return Files.exists(Paths.get(pathLocal.toString(), nameFile));
        }
        public boolean existFileHdfs(String path) throws IOException {
            return fileSystem.exists(new Path(path));
        }*/
        public boolean append(String[] parameters) {
            try {
                Path newPathHdfs = new Path((pathHdfs.toString().charAt(pathHdfs.toString().length() - 1) != '/'
                        ? pathHdfs.toString() + "/" : pathHdfs.toString()) + parameters[2]);
                Path newPathLocal = new Path((pathLocal.toString().charAt(pathLocal.toString().length() - 1) != '/'
                        ? pathLocal.toString() + "/" : pathLocal.toString()) + parameters[1]);
                FSDataInputStream fileHdfs =  fileSystem.open(newPathHdfs);
                BufferedReader bufferedReader = new BufferedReader(new FileReader(newPathLocal.toString()));
                FSDataOutputStream fileMerge = fileSystem.append(newPathHdfs);
                while (fileHdfs.available() > 0) fileMerge.write(fileHdfs.read());
                for(String text =bufferedReader.readLine(); text != null; text = bufferedReader.readLine())
                    fileMerge.write(text.getBytes(StandardCharsets.UTF_8));
                fileMerge.flush();
                fileHdfs.close();
                fileMerge.close();
            System.out.println("The file is merged!");
            } catch (Exception exception) {
                System.out.println(exception.getMessage());
            }
            return true;
        }
        public void showAllCommand(){
            String[] commands = new String[]{"cd", "ls", "lcd -local cd", "lls - local ls", "get", "put", "append", "delete"};
            System.out.println("Commands:");
            for (String command: commands) System.out.println("\t" + command);
        }
    }
}



